﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Vars
{
    public static int players = 1;
    public static bool vertLayout = false;
    public static string lang = "ENG";
}
