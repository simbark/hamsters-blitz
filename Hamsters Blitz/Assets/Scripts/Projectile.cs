﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour
{
    public Camera whoseCam;
    public string player;
    public float speed;
    public float damage;
    
    bool shot;
    Rigidbody rb;
    Vector3 force, scale;

    void Start()
    {
        rb = gameObject.GetComponent<Rigidbody>();
        scale = transform.localScale;
        transform.localScale = Vector3.zero;
    }

    void Update()
    {
        if(whoseCam && !shot)
        {
            shot = true;
            Ray ray = whoseCam.ViewportPointToRay(new Vector3(.5f,.5f,0));
            RaycastHit hit;
            if(Physics.Raycast(ray, out hit, 10000f))
            {
                transform.LookAt(hit.point, Vector3.right);
                transform.localScale = scale;
                force = (hit.point - transform.position);
            }
            else Destroy(gameObject);
        }
        if(shot)
        {
            rb.AddForce(force);
            rb.velocity = rb.velocity.normalized * speed;
        }
    }

    void OnTriggerEnter(Collider hit)
    {
        if(hit.gameObject.name!="BallTrigger" && hit.gameObject.name!="Objective")
        {
            if(hit.gameObject.tag=="Character")
            {
                Transform current = hit.transform;
                Character them = hit.gameObject.GetComponent<Character>();
                while(!them)
                {
                    current = current.parent;
                    them = current.gameObject.GetComponent<Character>();
                }
                if(hit.gameObject.name!="Hamsterball")
                {
                    them.Damage(damage);
                    if(them.health<=damage && player!="")
                        GameObject.Find("/Players/" + player).GetComponent<Player>().Earn(them.reward);
                }
                else if(player=="") them.Damage(damage);
                Destroy(gameObject);
            }
            if(hit.gameObject.tag!=player) Destroy(gameObject);
        }
    }
}
