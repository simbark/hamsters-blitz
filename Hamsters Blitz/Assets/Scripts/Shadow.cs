﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shadow : MonoBehaviour
{
    public Transform player;
    public bool canJump, canLeap;
    int shadowMask;
    float offset = .05f, rad = .5f;
    Vector3[] tests;

    void Start()
    {
        shadowMask = ~(LayerMask.GetMask("IgnoreShadows") | LayerMask.GetMask("Ignore Raycast") | LayerMask.GetMask("Not Player 1") | LayerMask.GetMask("Not Player 2") | LayerMask.GetMask("Not Player 3") | LayerMask.GetMask("Not Player 4"));
        Vector3 tp0 = new Vector3(0, offset, 0);
        Vector3 tp1 = new Vector3(-rad, offset, -rad);
        Vector3 tp2 = new Vector3(-rad, offset, rad);
        Vector3 tp3 = new Vector3(rad, offset, -rad);
        Vector3 tp4 = new Vector3(rad, offset, rad);
        tests = new Vector3[] {tp0, tp1, tp2, tp3, tp4};
    }

    void Update()
    {
        float shortestRayDist = 10000f;
        foreach(Vector3 p in tests)
        {
            Vector3 testPos = player.position + p;
            RaycastHit hit;
            if(Physics.Raycast(testPos, Vector3.down, out hit, shortestRayDist, shadowMask))
            {
                transform.position = new Vector3(player.position.x, player.position.y+offset-hit.distance, player.position.z);
                shortestRayDist = hit.distance;
            }
        }
        canJump = Mathf.Abs(transform.position.y-player.position.y)<.75;
        canLeap = Mathf.Abs(transform.position.y-player.position.y)<1.5;
        if(transform.rotation!=Quaternion.identity) transform.rotation = Quaternion.identity;
    }
}
