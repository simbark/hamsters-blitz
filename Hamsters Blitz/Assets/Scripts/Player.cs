﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Player : MonoBehaviour
{
    public int playerID;
    public float walkSpeed, camSpeed, jumpSpeed, sprintDuration;
    public Transform cam;
    public Transform axis;
    public Collider[] colliders;
    public Shadow shadow;
    public Weapon weapon;
    public Animator anim;
    public GameObject ball, ballPrefab, respawner;
    
    GameObject myBall;
    Vector3 moveDir;
    Transform camPiv;
    Cameraman zoomer;
    Rigidbody rb;
    Rigidbody ballRB;
    Transform myCanvas;
    RectTransform dashBarProg;
    Image reticle;
    CanvasGroup hamsterballDialogue, dashBarBack, ballRespawner;
    Text dashBarText, currency, message, ballRespawnTimerText;
    RectTransform[] hearts;

    float origAim, aimX, aimY, origSpeed, lastDash=-1, messageTimer, ballRespawnTimer;
    const int MID = 0, CLOSE = 1, FAR = 2;
    bool culled, floating, onBall, lastFireA, lastFireB, wasJumpable;
    public bool inBall;
    
    // Start is called before the first frame update
    void Start()
    {
        if(Vars.players<playerID || playerID<1) Destroy(gameObject);
        else
        {
            Cursor.visible = false;
            Cursor.lockState = CursorLockMode.Locked;
            origSpeed = walkSpeed;
            origAim = camSpeed;
            aimX = transform.eulerAngles.y;
            camPiv = cam.parent;

            zoomer = cam.gameObject.GetComponent<Cameraman>();
            rb = GetComponent<Rigidbody>();
            myCanvas = GameObject.Find("/Canvas/" + playerID).transform;
            reticle = myCanvas.Find("Reticle").gameObject.GetComponent<Image>();
            hamsterballDialogue = myCanvas.Find("Hamsterball").gameObject.GetComponent<CanvasGroup>();
            dashBarBack = myCanvas.Find("DashBar").gameObject.GetComponent<CanvasGroup>();
            dashBarProg = myCanvas.Find("DashBar/Prog").gameObject.GetComponent<RectTransform>();
            dashBarText = myCanvas.Find("DashBar/Text").gameObject.GetComponent<Text>();
            currency = myCanvas.Find("Seeds/Text").gameObject.GetComponent<Text>();
            message = myCanvas.Find("Message").gameObject.GetComponent<Text>();
            ballRespawnTimerText = myCanvas.Find("BallRespawnTimer").gameObject.GetComponent<Text>();
            ballRespawner = myCanvas.Find("BallRespawn").gameObject.GetComponent<CanvasGroup>();
            myBall = GameObject.Find("Hamsterball");
            myBall.GetComponent<Hamsterball>().owner = playerID;
            myBall.transform.parent = transform.Find("/Balls");
            
            hearts = new RectTransform[myCanvas.Find("Hearts").childCount];
            for(int i=0; i<hearts.Length; i++)
                hearts[i] = myCanvas.Find("Hearts").GetChild(i).GetComponent<RectTransform>();
        }
    }

    // Update is called once per frame
    void Update()
    {
        // Looking
        float axisX = Input.GetAxis("MoveX" + playerID);
        float axisY = Input.GetAxis("MoveY" + playerID);
        if(playerID==1)
        {
            aimX += Input.GetAxis("Mouse X") * camSpeed;
            aimY -= Input.GetAxis("Mouse Y") * camSpeed;
            aimX += Input.GetAxis("AimX" + playerID) * camSpeed * 5;
            aimY += Input.GetAxis("AimY" + playerID) * camSpeed * 5;
            axisX += Input.GetAxis("MoveX1k");
            axisY += Input.GetAxis("MoveY1k");
        }
        aimX += Input.GetAxis("AimX" + playerID) * camSpeed * 5;
        aimY += Input.GetAxis("AimY" + playerID) * camSpeed * 5;
        if(inBall) aimY = Mathf.Clamp(aimY, -45, 45);
        else aimY = Mathf.Clamp(aimY, -60, 60);
        camPiv.rotation = Quaternion.Euler(aimY, aimX, 0);

        // Moving
        Vector3 forward = cam.forward;
        Vector3 right = cam.right;
        forward.y = right.y = 0;
        forward.Normalize();
        right.Normalize();
        moveDir = (forward * axisY) + (right * axisX);
        moveDir = Vector3.ClampMagnitude(moveDir, 1f);
        if(zoomer.currentView==CLOSE) axis.rotation = Quaternion.LookRotation(forward);
        else if(moveDir!=Vector3.zero) axis.rotation = Quaternion.LookRotation(moveDir);
        
        // Jump & float
        Rigidbody trb = rb;
        bool jumpable = ((inBall && (onBall || shadow.canJump)) || (!inBall && shadow.canLeap));
        if(!floating && Input.GetButtonDown("Jump" + playerID) && jumpable)
        {
            anim.SetTrigger("jump");
            floating = true;
            float js = jumpSpeed;
            if(inBall)
            {
                trb = ballRB;
                js *= 1.5f;
            }
            trb.velocity = new Vector3(trb.velocity.x, js, trb.velocity.z);
            trb.AddForce(Vector3.up * 200);
        }
        else if(floating && (Input.GetButtonUp("Jump" + playerID) ||
               (!Input.GetButtonDown("Jump" + playerID) && rb.velocity.y==0)))
        {
            floating = false;
            if(inBall) trb = ballRB;
            trb.AddForce(Vector3.down * 200);
        }
        if(jumpable && !wasJumpable) anim.SetTrigger("land");
        if(!jumpable && wasJumpable) anim.SetTrigger("fall");
        wasJumpable = jumpable;

        // Dash timer
        if(lastDash>0 && dashBarBack.alpha==0) dashBarBack.alpha=1;
        else if(lastDash<=0 && dashBarBack.alpha!=0) dashBarBack.alpha=0;

        if(messageTimer>0) messageTimer -= Time.deltaTime;
        else if(message.text!="") message.text="";
        if(ballRespawnTimer>0)
        {
            ballRespawnTimer -= Time.deltaTime;
            if(!inBall)
            {
                if(Vars.lang=="JP") ballRespawnTimerText.text = "ボールリスポーン: <b>" + (int) ballRespawnTimer + "</b>";
                else ballRespawnTimerText.text = "Ball respawn: <b>" + (int) ballRespawnTimer + "</b>";
            }
            else if(ballRespawnTimerText.text!="") ballRespawnTimerText.text = "";
        }
        else
        {
            if(ballRespawnTimerText.text!="") ballRespawnTimerText.text = "";
            if(myBall==null)
            {
                if(hamsterballDialogue.alpha==1) ballRespawner.alpha=0;
                else if(ballRespawner.alpha==0 && !inBall) ballRespawner.alpha=1;
                if(ballRespawner.alpha==1 && Input.GetButtonDown("Respawn" + playerID))
                {
                    myBall = Instantiate(ballPrefab, transform.position, transform.rotation, GameObject.Find("/Balls").transform);
                    myBall.transform.position = new Vector3(myBall.transform.position.x, myBall.transform.position.y + 5, myBall.transform.position.z);
                    myBall.GetComponent<Hamsterball>().owner = playerID;
                    ballRespawner.alpha=0;
                }
            }
        }

        if(!inBall)
        {
            // Sprint
            if(Input.GetButtonDown("Sprint" + playerID) && lastDash<=-1)
            {
                lastDash = 1;
                anim.SetInteger("moveType", 2);
            }
            else if(Input.GetButtonUp("Sprint" + playerID) || lastDash<=0) 
            {
                walkSpeed = origSpeed;
                anim.SetInteger("moveType", 1);
            }
            if(lastDash>-1)
            {
                if(dashBarBack.alpha!=1) dashBarBack.alpha=1;
                lastDash -= Time.deltaTime / sprintDuration;
                if(lastDash>0)
                {
                    walkSpeed = origSpeed * 2;
                    dashBarProg.localScale = new Vector3(lastDash, 1f, 1f);
                }
                else
                {
                    dashBarProg.localScale = new Vector3(-lastDash, 1f, 1f);
                    walkSpeed = origSpeed;
                }
            }
            else
            {
                if(dashBarBack.alpha!=0) dashBarBack.alpha=0;
                if(Input.GetButtonDown("Sprint" + playerID)) lastDash = 1;
            }
            if(moveDir!=Vector3.zero)
            {
                if(walkSpeed==origSpeed) anim.SetInteger("moveType", 1);
                else anim.SetInteger("moveType", 2);
            }
            else anim.SetInteger("moveType", 0);

            // ADS
            if((playerID==1 && Input.GetButtonDown("FireB1k")) ||
               (Input.GetAxis("FireB" + playerID)!=0 && !lastFireB))
            {
                zoomer.currentView = CLOSE;
                reticle.color = Color.red;
                camSpeed = origAim/2;
            }
            else if((playerID==1 && Input.GetButtonUp("FireB1k")) ||
                    (Input.GetAxis("FireB" + playerID)==0 && lastFireB))
            {
                zoomer.currentView = MID;
                reticle.color = Color.clear;
                camSpeed = origAim;
            }
            lastFireB = Input.GetAxis("FireB" + playerID)!=0;

            // Shoot
            if((zoomer.currentView==CLOSE && ((playerID==1 && Input.GetButtonDown("FireA1k")) ||
               (Input.GetAxis("FireA" + playerID)!=0 && !lastFireA))) &&
              (Physics.Raycast(cam.gameObject.GetComponent<Camera>().ViewportPointToRay(new Vector3(.5f,.5f,0)), 10000f)))
                weapon.Fire(playerID);
            lastFireA = Input.GetAxis("FireA" + playerID)!=0;
            
            // Get in ball
            if(Input.GetButtonDown("Use" + playerID) && ball)
            {
                anim.SetInteger("moveType", 0);
                inBall = true; onBall = false;
                zoomer.currentView = FAR;
                hamsterballDialogue.alpha = 0;
                
                ball.GetComponent<SphereCollider>().enabled = false;
                foreach(Collider c in colliders) c.enabled = false;
                rb.isKinematic = true;
                
                if(Vars.lang=="JP") dashBarText.text = "駆ける";
                else dashBarText.text = "DASH";
                dashBarBack.alpha=0;

                myCanvas.Find("BallHealth").GetComponent<CanvasGroup>().alpha = 1;
                myCanvas.Find("BallHealth/Prog").GetComponent<RectTransform>().localScale = ball.transform.parent.GetComponent<Character>().GetHealth();
                
                if(walkSpeed!=origSpeed) walkSpeed = origSpeed;
                Vector3 ballPos = ball.transform.parent.position;
                transform.position = new Vector3(ballPos.x, ballPos.y-2.5f, ballPos.z);
            }
        }
        else if(inBall)
        {
            myCanvas.Find("BallHealth/Prog").GetComponent<RectTransform>().localScale = ball.transform.parent.GetComponent<Character>().GetHealth();

            // Roll ball & tilt camera
            ballRB.AddForce(moveDir * walkSpeed*500 * Time.deltaTime);
            camPiv.localEulerAngles = new Vector3(camPiv.localEulerAngles.x + axisY*-20f + 20f, camPiv.localEulerAngles.y, camPiv.localEulerAngles.z + axisX*10f);

            // Dash
            float maxHorizSpeed = 150f;
            dashBarProg.localScale = new Vector3(lastDash/2, 1f, 1f);
            if(Input.GetButtonDown("Sprint" + playerID) && lastDash<=0)
            {
                ballRB.AddForce(moveDir*maxHorizSpeed, ForceMode.Impulse);
                lastDash = sprintDuration;
            }
            Vector2 horizV = new Vector2(ballRB.velocity.x, ballRB.velocity.z);
            if(lastDash>0) lastDash -= Time.deltaTime;
            if(horizV.magnitude<1f) anim.SetInteger("moveType", 0);
            else if(horizV.magnitude<12.5) anim.SetInteger("moveType", 1);
            else if(horizV.magnitude<25) anim.SetInteger("moveType", 2);
            else anim.SetInteger("moveType", 3);

            // Get out
            if(Input.GetButtonDown("Use" + playerID)) GetOut();

            if(ball)
            {
                Vector3 ballPos = ball.transform.parent.position;
                transform.position = new Vector3(ballPos.x, ballPos.y-2.5f, ballPos.z);
            }
        }
    }

    public void GetOut()
    {
        anim.SetInteger("moveType", 0);
        inBall = false;
        zoomer.currentView = MID;
        lastDash = -1;
        rb.isKinematic = false;
        if(ball)
        {
            ball.GetComponent<SphereCollider>().enabled = true;
            hamsterballDialogue.alpha = 1;
        }
        foreach(Collider c in colliders) c.enabled = true;
        dashBarBack.alpha=0;
        if(Vars.lang=="JP") dashBarText.text = "走ている";
        else dashBarText.text = "SPRINT";
        myCanvas.Find("BallHealth").GetComponent<CanvasGroup>().alpha = 0;
    }

    // For proximity to hamsterballs
    void OnTriggerEnter(Collider trigger)
    {
        switch(trigger.gameObject.name)
        {
            case "BallTrigger":
                ball = trigger.gameObject;
                ballRB = ball.transform.parent.gameObject.GetComponent<Rigidbody>();
                hamsterballDialogue.alpha = 1;
                // hide message
                break;
            case "Hamsterball":
                onBall = true;
                break;
        }
    }
    void OnTriggerExit(Collider trigger)
    {
        switch(trigger.gameObject.name)
        {
            case "BallTrigger":
                ball = null;
                ballRB = null;
                hamsterballDialogue.alpha = 0;
                // show message if valid
                break;
            case "Hamsterball":
                onBall = false;
                break;
        }
    }
    void FixedUpdate()
    {
        float vertV = rb.velocity.y;
        if(vertV>40) vertV = 40;
        else if(vertV<-40) vertV = -40;
        rb.velocity = moveDir * walkSpeed * Time.deltaTime * 50;
        rb.velocity = new Vector3(rb.velocity.x, vertV, rb.velocity.z);
    }

    public void UpdateUIHealth(float h)
    {
        foreach(RectTransform r in hearts) r.localScale = new Vector2(1,1);
        if(h>80)
        {
            float scale = (h-80)/20;
            hearts[4].localScale = new Vector2(scale, scale);
        }
        else
        {
            hearts[4].localScale = new Vector2(0,0);
            if(h>60)
            {
                float scale = (h-60)/20;
                hearts[3].localScale = new Vector2(scale, scale);
            }
            else
            {
                hearts[3].localScale = new Vector2(0,0);
                if(h>40)
                {
                    float scale = (h-40)/20;
                    hearts[2].localScale = new Vector2(scale, scale);
                }
                else
                {
                    hearts[2].localScale = new Vector2(0,0);
                    if(h>20)
                    {
                        float scale = (h-20)/20;
                        hearts[1].localScale = new Vector2(scale, scale);
                    }
                    else
                    {
                        hearts[1].localScale = new Vector2(0,0);
                        if(h>0)
                        {
                            float scale = h/20;
                            hearts[0].localScale = new Vector2(scale, scale);
                        }
                        else hearts[0].localScale = new Vector2(0,0);
                    }
                }
            }
        }
    }

    public void Earn(int s)
    {
        Manager m = GameObject.Find("/EventSystem").GetComponent<Manager>();
        m.currencies[playerID-1] += s;
        currency.text = "× " + m.currencies[playerID-1];
    }

    public void BallDied()
    {
        if(Vars.lang=="JP") message.text = "お前のハムスターボールは破壊されました。";
        else message.text = "Your hamsterball was destroyed.";
        messageTimer = 5;
        ballRespawnTimer = 20;
    }

    public void Kill()
    {
        Destroy(myBall);
        GameObject newHost = Instantiate(respawner, transform.Find("/Objects"));
        transform.Find("CamPivot/Camera").parent = newHost.transform;
        newHost.GetComponent<Respawner>().SetID(playerID);

        reticle.color = Color.clear;
        foreach(RectTransform r in hearts) r.localScale = new Vector2(0,0);
        ballRespawnTimerText.text="";
        ballRespawner.alpha=0;
        hamsterballDialogue.alpha=0;
        myCanvas.Find("BallHealth").GetComponent<CanvasGroup>().alpha=0;
        dashBarBack.alpha=0;
        zoomer.enabled = false;
    }
}
