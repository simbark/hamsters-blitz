﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hamsterball : MonoBehaviour
{
    public SphereCollider coll;
    public int owner;

    Rigidbody rb;
    Transform[] players;
    float maxSpeed = 150f, bouncy = .5f;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
        GetComponent<Character>().Damage(0);
        BarsFollowCameras();
    }

    void Update()
    {
        BarsFollowCameras();
        float bounce = rb.velocity.magnitude/maxSpeed + bouncy;
        if(bounce>.8f) bounce = .8f;
        else if(rb.velocity.magnitude<.1f) bounce = 0;
        coll.material.bounciness = bounce;
        Vector2 horizV = new Vector2(rb.velocity.x, rb.velocity.z);
        if(horizV.magnitude>maxSpeed)
        {
            horizV = Vector2.ClampMagnitude(horizV, maxSpeed);
            rb.velocity = new Vector3(horizV.x, rb.velocity.y, horizV.y);
        }
    }

    void BarsFollowCameras()
    {
        transform.Find("Canvases").eulerAngles = Vector3.zero;
        transform.Find("Canvases").position = new Vector3(transform.position.x, transform.position.y+.75f, transform.position.z);
        players = new Transform[GameObject.Find("/Players").transform.childCount];
        for(int i=0; i<players.Length; i++)
        {
            players[i] = GameObject.Find("/Players/Player" + (i+1)).transform;
            Transform canvas = transform.Find("Canvases/" + (i+1));
            canvas.LookAt(2 * canvas.position - players[i].Find("CamPivot/Camera").position);
        }
    }

    public void Kill()
    {
        players[owner-1].gameObject.GetComponent<Player>().BallDied();
    }
}
