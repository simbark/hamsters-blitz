﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Animations;

public class Cameraman : MonoBehaviour
{
    public Player player;
    public int currentView;
    public Transform[] views;
    public float[] fovs;
    public Vector3 offset;

    int mask, fpsMask;
    Camera cam;
    RectTransform ui;

    Image img; Text dist; GameObject jar;

    void Start()
    {
        cam = GetComponent<Camera>();
        Transform myCanvas = GameObject.Find("Canvas/"+player.playerID).transform;
        ui = myCanvas.gameObject.GetComponent<RectTransform>();
        img = myCanvas.Find("Waypoint").gameObject.GetComponent<Image>();
        dist = myCanvas.Find("Waypoint/Dist").gameObject.GetComponent<Text>();
        mask = cam.cullingMask | (1 << 9+player.playerID);
        cam.cullingMask = mask;
        fpsMask = mask & ~(1 << 17+player.playerID);

        if(Vars.players!=1) 
        {
            ui.localScale = new Vector3(.5f, .5f, 1);
            switch(Vars.players)
            {
                case 2:
                    if(Vars.vertLayout) ui.anchorMax = new Vector2(1, 2);
                    else ui.anchorMax = new Vector2(2, 1);
                    if(player.playerID==1)
                    {
                        if(Vars.vertLayout)
                        {
                            cam.rect = new Rect(0, 0, .5f, 1f);
                        }
                        else
                        {
                            cam.rect = new Rect(0, .5f, 1f, .5f);
                            ui.pivot = new Vector2(0, 1);
                        }
                    }
                    else
                    {
                        if(Vars.vertLayout)
                        {
                            cam.rect = new Rect(.5f, 0, .5f, 1f);
                            ui.pivot = new Vector2(1, 0);
                        }
                        else
                        {
                            cam.rect = new Rect(0, 0, 1f, .5f);
                        }
                    }
                    break;
                case 3:
                    if(player.playerID==1)
                    {
                        if(Vars.vertLayout)
                        {
                            cam.rect = new Rect(0, 0, .5f, 1f);
                            ui.anchorMax = new Vector2(1, 2);
                        }
                        else 
                        {
                            cam.rect = new Rect(0, .5f, 1f, .5f);
                            ui.anchorMax = new Vector2(2, 1);
                            ui.pivot = new Vector2(0, 1);
                        }
                    }
                    else if(player.playerID==2)
                    {
                        if(Vars.vertLayout)
                        {
                            cam.rect = new Rect(.5f, .5f, .5f, .5f);
                            ui.pivot = new Vector2(1, 1);
                        }
                        else
                        {
                            cam.rect = new Rect(0, 0, .5f, .5f);
                        }
                    }
                    else
                    {
                        cam.rect = new Rect(.5f, 0, .5f, .5f);
                        ui.pivot = new Vector2(1, 0);
                    }
                    break;
                case 4:
                    if(player.playerID==1)
                    {
                        cam.rect = new Rect(0, .5f, .5f, .5f);
                        ui.pivot = new Vector2(0, 1);
                    }
                    else if(player.playerID==2)
                    {
                        cam.rect = new Rect(.5f, .5f, .5f, .5f);
                        ui.pivot = new Vector2(1, 1);
                    }
                    else if(player.playerID==3)
                    {
                        cam.rect = new Rect(0, 0, .5f, .5f);
                    }
                    else
                    {
                        cam.rect = new Rect(.5f, 0, .5f, .5f);
                        ui.pivot = new Vector2(1, 0);
                    }
                    break;
            }
        } 
    }
    // Update is called once per frame
    void Update()
    {
        float minX = img.GetPixelAdjustedRect().width/2;
        float maxX = ui.rect.width - minX;

        float minY = img.GetPixelAdjustedRect().height/2;
        float maxY = ui.rect.height - minY;

        if(jar==null) jar = GameObject.Find("Objective");
        Vector2 pos = GetComponent<Camera>().WorldToScreenPoint(jar.transform.position + offset);

        if(Vector3.Dot((jar.transform.position - transform.position), transform.forward)<0)
        {
            if(pos.x < ui.rect.width/2) pos.x = maxX;
            else pos.x = minX;
        }
        
        pos.x = Mathf.Clamp(pos.x, minX, maxX);
        pos.y = Mathf.Clamp(pos.y, minY, maxY);

        img.transform.position = pos;
        dist.text = ((int) (Vector3.Distance(jar.transform.position, transform.position)/20)).ToString() + "m";

        // - - - - - - - - - -

        if(currentView==2 && cam.cullingMask!=fpsMask) cam.cullingMask=fpsMask;
        else if(currentView!=2 && cam.cullingMask!=mask) cam.cullingMask=mask;

        RaycastHit hit;
        Vector3 targetPosition; Vector3 desiredPosition = views[currentView].position;
        Vector3 vecDiff = desiredPosition-transform.parent.position;
        if(Physics.Raycast(transform.parent.position, vecDiff.normalized, out hit, vecDiff.magnitude, 1<<17))
        {
            targetPosition = Vector3.MoveTowards(hit.point,transform.parent.position,.25f);
            transform.position = targetPosition;
        }
        else targetPosition = desiredPosition;

        float moveSpeed = Vector3.Distance(transform.position, targetPosition);
        if(moveSpeed>.001f)
        {
            transform.position = Vector3.MoveTowards(transform.position, targetPosition, moveSpeed * Time.deltaTime / .1f);
            transform.localRotation = Quaternion.RotateTowards(transform.localRotation, views[currentView].localRotation, 5 * Time.deltaTime / .1f);

            float diff = Mathf.Abs(fovs[currentView]-cam.fieldOfView);
            if(cam.fieldOfView < fovs[currentView]) cam.fieldOfView += diff * Time.deltaTime / .1f;
            else cam.fieldOfView -= diff * Time.deltaTime / .1f;
        }
        else
        {
            transform.position = targetPosition;
            transform.localRotation = views[currentView].localRotation;
            cam.fieldOfView = fovs[currentView];
        }
    }
}
