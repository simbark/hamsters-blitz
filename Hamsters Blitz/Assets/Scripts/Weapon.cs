﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Weapon : MonoBehaviour
{
    public GameObject projectile;

    public void Fire(int p)
    {
        GameObject newProjectile = Instantiate(projectile, transform.Find("/Objects"));
        newProjectile.transform.position = transform.position;
        newProjectile.GetComponent<Projectile>().whoseCam = transform.parent.parent.Find("CamPivot/Camera").gameObject.GetComponent<Camera>();
        newProjectile.GetComponent<Projectile>().player = "Player" + p;
    }
}
