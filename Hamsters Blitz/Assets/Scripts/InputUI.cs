﻿ using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InputUI : MonoBehaviour
{
    public Sprite xButton, yButton, eKey, qKey;
    public Image[] hamsterball, respawn;
    bool wasController, firedA, firedB;

    void Start()
    {
        MethodChange(false);
    }

    void Update()
    {
        bool buttonCheck = false;
        for(int i=0; i<20; i++) {
            if(Input.GetKeyDown("joystick 1 button " + i))
            {
                buttonCheck = true;
                break;
            }
        }

        bool controller = wasController;
        if(Input.GetAxis("Mouse X")!=0 || Input.GetAxis("Mouse Y")!=0)
              controller = false;
        else if(controller && (Input.anyKeyDown ||
           Input.GetAxis("MoveX1k")!=0 ||
           Input.GetAxis("MoveY1k")!=0))
              controller = false;
        else if(buttonCheck ||
           Input.GetAxis("MoveX1")!=0 ||
           Input.GetAxis("MoveY1")!=0 ||
           Input.GetAxis("AimX1")!=0 ||
           Input.GetAxis("AimY1")!=0 ||
           (Input.GetAxis("FireA1")!=0)!=firedA ||
           (Input.GetAxis("FireB1")!=0)!=firedB)
              controller = true;
        if(controller!=wasController) MethodChange(controller);
        firedA = Input.GetAxis("FireA1")!=0;
        firedB = Input.GetAxis("FireB1")!=0;
        wasController = controller;
    }

    void MethodChange(bool controller)
    {
        if(controller)
        {
            foreach(Image i in hamsterball) i.sprite = xButton;
            foreach(Image i in respawn) i.sprite = yButton;
        }
        else
        {
            foreach(Image i in hamsterball) i.sprite = eKey;
            foreach(Image i in respawn) i.sprite = qKey;
        }
    }
}
