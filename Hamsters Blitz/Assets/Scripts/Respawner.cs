﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Respawner : MonoBehaviour
{
    bool going;
    public int id;
    public GameObject player;
    float countdown = 10;
    Transform myCanvas;
    Text ko, message;
    Manager manager;

    void Start()
    {
        manager = GameObject.Find("/EventSystem").GetComponent<Manager>();
    }

    void Update()
    {
        if(going)
        {
            if(Vars.lang=="JP") message.text = "<b>" + (int) countdown + "</b>でリスポーン";
            else message.text = "Respawn in <b>" + (int) countdown + "</b>";
            if(countdown<=5 && ko.text!="") ko.text="";

            if(countdown<=0)
            {
                ko.text="";
                message.text="";
                manager.SpawnPlayer(id);
                Destroy(gameObject);
            }
            countdown -= Time.deltaTime;
        }
    }

    public void SetID(int x)
    {
        id = x;
        going = true;

        myCanvas = GameObject.Find("/Canvas/" + id).transform;
        ko = myCanvas.Find("KO").gameObject.GetComponent<Text>();
        message = myCanvas.Find("Message").gameObject.GetComponent<Text>();

        if(Vars.lang=="JP") ko.text = "死";
        else ko.text = "K.O.";
    }
}
