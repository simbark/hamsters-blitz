﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.SceneManagement;

public class Character : MonoBehaviour
{
    public float health = 100f;
    public int reward = 0;
    float maxHealth;
    float damageCooldown;
    float damageRate = .1f;
    public string type;
    static Manager manager;
    bool destroyed, wasCarrying;
    public bool carrying;
    RectTransform[] ballBars;
    Vector3 lastPathPos, carryPos;
    GameObject objective;

    void Start()
    {
        maxHealth = health;
        manager = GameObject.Find("/EventSystem").GetComponent<Manager>();
        if(type=="Player") gameObject.GetComponent<Player>().UpdateUIHealth(health);
        if(type=="Ball")
        {
            ballBars = new RectTransform[4];
            for(int i=0; i<4; i++)
            {
                ballBars[i] = GameObject.Find("Canvases/" + (i+1)+ "/DashBar/Prog").GetComponent<RectTransform>();
                ballBars[i].localScale = GetHealth();
            }
        }
        if(type!="Enemy") InvokeRepeating("PathCheck", 0f, .1f);
        else objective = GameObject.Find("/Objective");
    }

    void Update()
    {
        if(damageCooldown<=damageRate) damageCooldown += Time.deltaTime;
        if(type=="Enemy")
        {
            if(carrying)
            {
                if(wasCarrying) transform.position = objective.transform.position + carryPos;
                else carryPos = objective.transform.InverseTransformPoint(transform.position);
            }
            wasCarrying = carrying;
        }
    }

    public void Damage(float d)
    {
        if(damageCooldown>damageRate)
        {
            damageCooldown=0;
            health -= d;
            if(type=="Player") gameObject.GetComponent<Player>().UpdateUIHealth(health);
            if(type=="Ball") foreach(RectTransform r in ballBars) r.localScale = GetHealth();
            if(health<=0 && !destroyed)
            {
                if(type=="Player") GetComponent<Player>().Kill();
                else if(type=="Enemy") 
                {
                    if(carrying) GameObject.Find("/Objective").GetComponent<Objective>().carriers--;
                    manager.Subtract();
                }
                else if(type=="Ball")
                {
                    int playerCount = GameObject.Find("/Players").transform.childCount;
                    GameObject[] players = new GameObject[playerCount];
                    for(int i=0; i<playerCount; i++) players[i] = GameObject.Find("/Players").transform.GetChild(i).gameObject;
                    bool found = false;
                    foreach(GameObject p in players)
                    {
                        Player player = p.GetComponent<Player>();
                        if(!found && player.inBall && player.ball.transform.parent.gameObject==gameObject)
                        {
                            found = true;
                            player.ball = null;
                            player.GetOut();
                        }
                    }
                    GetComponent<Hamsterball>().Kill();
                }
                destroyed=true;
                Destroy(gameObject);
            }
        }
    }

    public Vector2 GetHealth()
    {
        return new Vector2(health/maxHealth, 1);
    }

    void PathCheck()
    {
        Vector3 calcFrom = transform.position;
        if(type=="Ball") calcFrom = new Vector3(transform.position.x, transform.position.y-2.5f, transform.position.z);
        NavMeshPath path = new NavMeshPath();
        NavMesh.CalculatePath(calcFrom, GameObject.Find("/EnemySpawns").transform.GetChild(0).position, NavMesh.AllAreas, path);
        if(path.status==NavMeshPathStatus.PathComplete) lastPathPos = calcFrom;
    }

    public Vector3 GetPos()
    {
        return lastPathPos;
    }
}
