﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Enemy : MonoBehaviour
{
    public Animator anim;
    public NavMeshAgent agent;
    
    Vector3 goTo;
    GameObject target, players, balls;
    bool wasonLink;
    float thresh=50, punchCooldown, mover=1;
    float intelligence = .95f;

    void Start()
    {
        players = GameObject.Find("/Players");
        balls = GameObject.Find("/Balls");
        target = GameObject.Find("/Objective");
    }

    void Update()
    {
        if(!target || target==null || target==GameObject.Find("/Objective")) target = GameObject.Find("/Objective");
        if(!Fighting() && target!=GameObject.Find("/Objective")) target = GameObject.Find("/Objective");
        if(Fighting())
        {
            goTo = target.GetComponent<Character>().GetPos();
            float dist = Vector3.Distance(transform.position, target.transform.position);
            if((target.name.Substring(0,1)=="P" && dist<2.5f && punchCooldown<=0) ||
               (target.name.Substring(0,1)=="H" && dist<7.5f && punchCooldown<=0))
               {
                   punchCooldown=.75f;
                   target.GetComponent<Character>().Damage(10);
               }
            if(Vector3.Distance(transform.position, target.GetComponent<Character>().GetPos())>thresh) target=null;
        }
        else goTo = target.transform.position;
        if(punchCooldown>0) punchCooldown -= Time.deltaTime;
        if(mover<=.5f) mover += Time.deltaTime;
        if(agent.remainingDistance<.1f || mover>.5f) Pathfind();

        // Jump animation
        if(agent.isOnOffMeshLink && !wasonLink) anim.SetTrigger("link");
        wasonLink = agent.isOnOffMeshLink;

        // Scan for threats
        float closestTargetDist = Mathf.Infinity;
        GameObject[] scan = new GameObject[2] {players, balls};
        foreach(GameObject s in scan)
            for(int i=0; i<s.transform.childCount; i++)
            {
                GameObject obj = s.transform.GetChild(i).gameObject;
                if(obj==target) target=null;
                if(s!=players || (s==players && !obj.GetComponent<Player>().inBall))
                {
                    float dist = Vector3.Distance(transform.position, obj.GetComponent<Character>().GetPos());
                    if(dist<thresh && dist<closestTargetDist)
                    {
                        closestTargetDist = dist;
                        target = obj;
                    }
                }
            }
    }

    void Pathfind()
    {
        if(Random.Range(0f,1f)>intelligence)
        {
            mover=0;
            Vector2 horizPos = new Vector2(transform.position.x, transform.position.z);
            bool pathFound = false;
            int attempts=0;
            while(!pathFound)
            {
                Vector2 randomPos = horizPos + Random.insideUnitCircle.normalized * 5;
                Vector3 posSpace = new Vector3(randomPos.x, transform.position.y, randomPos.y);
                if((CalculatePathLength(posSpace,goTo)<CalculatePathLength(transform.position,goTo) && (CalculatePathLength(transform.position,posSpace)<10)) || attempts>=5)
                {
                    pathFound = true;
                    if(attempts>=5) posSpace = goTo;
                    agent.destination = posSpace;
                }
                attempts++;
            }
        }
        else agent.destination = goTo;
    }

    float CalculatePathLength(Vector3 start, Vector3 end)
    {
        NavMeshPath path = new NavMeshPath();
        NavMesh.CalculatePath(start, end, NavMesh.AllAreas, path);
        Vector3[] allWayPoints = new Vector3[path.corners.Length + 2];
        allWayPoints[0] = start;
        allWayPoints[allWayPoints.Length - 1] = end;
        for(int i = 0; i < path.corners.Length; i++) allWayPoints[i + 1] = path.corners[i];
        float pathLength = 0;
        for(int i = 0; i < allWayPoints.Length - 1; i++) pathLength += Vector3.Distance(allWayPoints[i], allWayPoints[i + 1]);
        return pathLength;
    }

    bool Fighting()
    {
        if(target==null) return false;
        else return target.name.Substring(0,1)=="P" || target.name.Substring(0,1)=="H";
    }
}
