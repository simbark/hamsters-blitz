﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Menu : MonoBehaviour
{
    public Dropdown dropdown;
    //public Toggle toggle;

    void Start()
    {
        Cursor.visible = true;
        Cursor.lockState = CursorLockMode.None;
    }

    void Update()
    {
        Vars.players = dropdown.value + 1;
        //Vars.vertLayout = toggle.isOn;
    }

    public void StartButton()
    {
        SceneManager.LoadScene("Simon's Scene");
    }
}
