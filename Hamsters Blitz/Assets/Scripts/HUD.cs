﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HUD : MonoBehaviour
{
    public Text hamsterball, wave, sprint, respawn;
    public Image[] engButtons, jpButtons;

    void Start()
    {
        if(Vars.lang=="JP")
        {
            foreach(Image i in engButtons) i.color = Color.clear;
            foreach(Image i in jpButtons) i.color = Color.white;
            hamsterball.text = "      <b>ハムスターボール</b>に入る";
            wave.text = "ウェーブ 1";
            sprint.text = "走ている";
            respawn.text = "      <b>ハムスターボール</b>をリスポーンする";
        }
    }
}