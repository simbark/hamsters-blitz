﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Manager : MonoBehaviour
{
    public GameObject defaultCamera;
    public GameObject uiPrefab;
    public GameObject playerPrefab;
    public GameObject enemyPrefab;
    public GameObject jarPrefab;
    public Transform playerSpawns;
    public Transform enemySpawns;
    public Transform jarSpawns;
    public int[] currencies;

    int enemyCap, waveEnemies, remainingEnemies, wave;
    Transform[] playerSpawnPoints;
    Transform[] enemySpawnPoints;
    Transform[] jarSpawnPoints;
    Transform[] jarEscapePoints;
    Transform[] canvases;
    GameObject jar;
    
    void Start()
    {
        if(defaultCamera) Destroy(defaultCamera);
        playerSpawnPoints = new Transform[playerSpawns.childCount];
        enemySpawnPoints = new Transform[enemySpawns.childCount];
        jarSpawnPoints = new Transform[jarSpawns.childCount];
        jarEscapePoints = new Transform[jarSpawnPoints.Length];
        canvases = new Transform[Vars.players];
        currencies = new int[Vars.players];
        for(int i=0; i<playerSpawns.childCount; i++) playerSpawnPoints[i] = playerSpawns.GetChild(i);
        for(int i=0; i<enemySpawns.childCount; i++) enemySpawnPoints[i] = enemySpawns.GetChild(i);
        for(int i=0; i<jarSpawns.childCount; i++)
        {
            jarSpawnPoints[i] = jarSpawns.GetChild(i);
            jarEscapePoints[i] = jarSpawns.GetChild(i).GetChild(0);
        }

        int[] takenSpawns = new int[Vars.players];
        for(int i=1; i<=Vars.players; i++)
        {
            int pickedSpawn = UnityEngine.Random.Range(0, playerSpawnPoints.Length);
            if(i>1) while(Array.IndexOf(takenSpawns,pickedSpawn)>-1) pickedSpawn = UnityEngine.Random.Range(0, playerSpawnPoints.Length);
            takenSpawns[i-1] = pickedSpawn;
            Transform spawnPoint = playerSpawnPoints[pickedSpawn];
            GameObject newPlayer = Instantiate(playerPrefab, spawnPoint.position, spawnPoint.rotation, GameObject.Find("Players").transform);
            newPlayer.name = "Player" + i;
            newPlayer.GetComponent<Player>().playerID = i;
            GameObject newUI = Instantiate(uiPrefab, GameObject.Find("Canvas").transform);
            newUI.name = "" + i;
            canvases[i-1] = newUI.transform;
            currencies[i-1] = 0;
        }
        GameObject.Find("Canvas/1").GetComponent<InputUI>().enabled = true;
        NewWave();
        jar = GameObject.Find("/Objective");
    }

    void Update()
    {
        if(jar!=null && GameObject.Find("Enemies").transform.childCount<enemyCap && remainingEnemies>=enemyCap)
        {
            Transform spawnPoint = enemySpawnPoints[UnityEngine.Random.Range(0, enemySpawnPoints.Length)];
            Instantiate(enemyPrefab, spawnPoint.position, spawnPoint.rotation, GameObject.Find("Enemies").transform);
        }
    }

    void NewWave()
    {
        wave++;
        enemyCap = Vars.players * 25;
        waveEnemies = enemyCap * 2 + (wave * 25 - 50);
        remainingEnemies = waveEnemies;
        foreach(Transform c in canvases)
        {
            if(Vars.lang=="JP") c.Find("Wave").gameObject.GetComponent<Text>().text = "ウェーブ " + wave;
            else c.Find("Wave").gameObject.GetComponent<Text>().text = "Wave " + wave;
            c.Find("Enemies").gameObject.GetComponent<Text>().text = "" + remainingEnemies + "/" + waveEnemies;
        }
        jar = GameObject.Find("/Objective");
        if(jar!=null) Destroy(jar);
        int spawnIndex = UnityEngine.Random.Range(0, jarSpawnPoints.Length);
        Transform spawnPoint = jarSpawnPoints[spawnIndex];
        jar = Instantiate(jarPrefab, spawnPoint.position, spawnPoint.rotation);
        jar.name = "Objective";
        jar.GetComponent<Objective>().SetDestination(jarEscapePoints[spawnIndex].position);
    }

    public void Subtract()
    {
        remainingEnemies--;
        if(remainingEnemies==0) NewWave();
        else foreach(Transform c in canvases)
            c.Find("Enemies").gameObject.GetComponent<Text>().text = "" + remainingEnemies + "/" + waveEnemies;
    }

    public void SpawnPlayer(int id)
    {
        int pickedSpawn = UnityEngine.Random.Range(0, playerSpawnPoints.Length);
        Transform spawnPoint = playerSpawnPoints[pickedSpawn];
        GameObject newPlayer = Instantiate(playerPrefab, spawnPoint.position, spawnPoint.rotation, GameObject.Find("Players").transform);
        newPlayer.name = "Player" + id;
        newPlayer.GetComponent<Player>().playerID = id;
    }
}
