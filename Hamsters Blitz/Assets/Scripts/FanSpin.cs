﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FanSpin : MonoBehaviour
{
    void Update()
    {
        float newRotation = transform.eulerAngles.y + 360 * Time.deltaTime;
        transform.eulerAngles = new Vector3(0, newRotation, 0);
    }
}
