﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Objective : MonoBehaviour
{
    public float speed;
    public int carriers;
    bool moving;

    float calcDelay, totalDist;
    NavMeshAgent agent;
    GameObject model;
    Vector3 offset;
    Transform canvasesParent;
    Transform[] canvases;

    void Start()
    {
        canvasesParent = GameObject.Find("/Canvas").transform;
    }

    public void SetDestination(Vector3 pos)
    {
        agent = GetComponent<NavMeshAgent>();
        model = GameObject.Find("Model");
        offset = new Vector3(0,1,0);
        agent.destination = pos;
    }

    void Update()
    {
        if(canvases==null && canvasesParent.childCount>0)
        {
            canvases = new Transform[Vars.players];
            for(int i=0; i<Vars.players; i++) canvases[i] = canvasesParent.GetChild(i);
        }
        if(carriers>0)
        {
            if(!moving)
            {
                moving = true;
                agent.speed = speed;
                model.transform.localPosition = offset;
                foreach(Transform c in canvases)
                    c.Find("JarProgress").gameObject.GetComponent<CanvasGroup>().alpha=1;
            }
            if(totalDist>0)
            {
                float remainingDist = Vector3.Distance(transform.position, agent.destination);
                float hudPos = (1000 * remainingDist/totalDist) - 500;
                if(hudPos>500) hudPos=500;
                foreach(Transform c in canvases)
                    c.Find("JarProgress/Jar").localPosition = new Vector3(hudPos,75,0);
            }
            else totalDist = Vector3.Distance(transform.position, agent.destination);
            if(agent.remainingDistance<1) SceneManager.LoadScene("MenuTest");
        }
        else if(moving)
        {
            moving = false;
            agent.speed = 0;
            model.transform.localPosition = Vector3.zero;
            foreach(Transform c in canvases)
                c.Find("JarProgress").gameObject.GetComponent<CanvasGroup>().alpha=0;
        }
    }

    void OnTriggerEnter(Collider trigger)
    {
        GameObject other = trigger.transform.parent.gameObject;
        if(trigger.gameObject.tag=="Enemy" && !other.GetComponent<Character>().carrying)
        {
            other.GetComponent<NavMeshAgent>().enabled = false;
            other.GetComponent<Character>().carrying = true;
            other.GetComponent<Enemy>().enabled = false;
            carriers++;
        }
    }
}